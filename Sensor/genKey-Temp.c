#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

#define N_temperature 100
#define LENGTH_K 32


void makeKey(){
    FILE *k;
    k = fopen("../key", "w");

    char str[] = "0123456789abcdef";
    srand(time(NULL));
    int i;
    for (i=0; i<LENGTH_K; i++){
        fprintf(k,"%c",str[rand() % 16]);
        srand(rand());
    }
    fclose(k);
}

void makeTemp(){
    FILE *f;
    f = fopen("../temperatures.txt", "w");
    int i;
    for (i = 0; i < N_temperature; i++){
        fprintf(f,"%d\n", rand() % 100);
    }
    fclose(f);  
}

int main() {
    makeKey();
    makeTemp();
    return 0;
}
