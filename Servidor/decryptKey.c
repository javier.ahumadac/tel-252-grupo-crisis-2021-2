#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

#define LEN 32

char key[LEN];
void get(){
    // PARA MANDAR TEMPERATURAS Y KEYS CIFRADOS
    system("git pull origin main");
}
void getKey(){
    FILE *k;
    k = fopen("../key", "w");
    int i;
    for (i=0; i<LEN; i++){
        fprintf(k,"%c",key[i]);
        srand(rand());
    }
    fclose(k);
}

// ASCII https://www.cprograms4future.com/p/
void asciiDecrypt(int num[LEN]){
    //Prints ascii value of given string
    int i;
    printf("Decrypted ASCII code\n");
    for(i=0; i<LEN; i++) {  
        key[i] = (char) num[i];
    }
    for(i=0; i<LEN; i++) {  
        printf("%c", key[i]);
    }
    printf("\n");
    getKey();
}



int main(){
    get();

    char KEY[100];
    char delim[] = " ";
    int dec[LEN];

    FILE *Key_enc;
    Key_enc = fopen("../key", "r");
    fgets (KEY, 100, Key_enc);

    int i;
    char *ptr = strtok(KEY, delim);
    fclose(Key_enc);
    printf("dec: ");
    for (i=0; i<32; i++){
        dec[i] = atoi(ptr);
        ptr = strtok(NULL, delim);
        printf("%d ", dec[i]);
    }
    printf("\n");
    asciiDecrypt(dec);

    return 0;
}
